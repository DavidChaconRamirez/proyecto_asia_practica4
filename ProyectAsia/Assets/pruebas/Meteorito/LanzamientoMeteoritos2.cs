﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanzamientoMeteoritos2 : MonoBehaviour
{
    public Transform prefab;
    public GameObject list;
    private bool verdad = true;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (verdad == true)
        {
            StartCoroutine(WaitAndPrint());
            verdad = false;
        }

    }

    private IEnumerator WaitAndPrint()
    {
        Instantiate(prefab, list.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(9f);
        verdad = true;


    }
}
