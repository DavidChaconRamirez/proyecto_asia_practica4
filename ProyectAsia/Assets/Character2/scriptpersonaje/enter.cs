﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enter : MonoBehaviour
{
    public GameObject objeto;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            objeto.SetActive(true);
            Destroy(gameObject);
        }
    }
}
