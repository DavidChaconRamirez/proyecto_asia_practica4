<Q                         DIRLIGHTMAP_OFF    DYNAMICLIGHTMAP_OFF    LIGHTMAP_OFF   SHADOWS_DEPTH      SHADOWS_SOFT   SPOT     &  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	vec4 _TimeEditor;
uniform 	float _Amplitude;
uniform 	float _Windspeed;
uniform 	vec4 __ST;
UNITY_LOCATION(5) uniform  sampler2D _;
in  vec4 in_POSITION0;
in  vec3 in_NORMAL0;
in  vec4 in_TANGENT0;
in  vec2 in_TEXCOORD0;
in  vec2 in_TEXCOORD1;
in  vec2 in_TEXCOORD2;
in  vec4 in_COLOR0;
out vec2 vs_TEXCOORD0;
out vec2 vs_TEXCOORD1;
out vec2 vs_TEXCOORD2;
out vec4 vs_TEXCOORD3;
out vec3 vs_TEXCOORD4;
out vec3 vs_TEXCOORD5;
out vec3 vs_TEXCOORD6;
out vec4 vs_COLOR0;
out vec4 vs_TEXCOORD7;
out vec4 vs_TEXCOORD8;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
vec3 u_xlat3;
float u_xlat8;
float u_xlat13;
void main()
{
    u_xlat0.xy = in_POSITION0.yy * hlslcc_mtx4x4unity_ObjectToWorld[1].xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_ObjectToWorld[0].xy * in_POSITION0.xx + u_xlat0.xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_ObjectToWorld[2].xy * in_POSITION0.zz + u_xlat0.xy;
    u_xlat0.xy = hlslcc_mtx4x4unity_ObjectToWorld[3].xy * in_POSITION0.ww + u_xlat0.xy;
    u_xlat8 = _Time.y + _TimeEditor.y;
    u_xlat0.xy = vec2(vec2(_Windspeed, _Windspeed)) * vec2(u_xlat8) + u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy * __ST.xy + __ST.zw;
    u_xlat0 = textureLod(_, u_xlat0.xy, 0.0);
    u_xlat1.xyz = in_COLOR0.xyz * vec3(_Amplitude);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz + in_POSITION0.xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat1 = hlslcc_mtx4x4unity_ObjectToWorld[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD1.xy = in_TEXCOORD1.xy;
    vs_TEXCOORD2.xy = in_TEXCOORD2.xy;
    vs_TEXCOORD3 = u_xlat0;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat1.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    vs_TEXCOORD4.xyz = u_xlat1.xyz;
    u_xlat2.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_TANGENT0.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_TANGENT0.zzz + u_xlat2.xyz;
    u_xlat13 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat2.xyz = vec3(u_xlat13) * u_xlat2.xyz;
    vs_TEXCOORD5.xyz = u_xlat2.xyz;
    u_xlat3.xyz = u_xlat1.zxy * u_xlat2.yzx;
    u_xlat1.xyz = u_xlat1.yzx * u_xlat2.zxy + (-u_xlat3.xyz);
    u_xlat1.xyz = u_xlat1.xyz * in_TANGENT0.www;
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    vs_TEXCOORD6.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    vs_COLOR0 = in_COLOR0;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD7 = hlslcc_mtx4x4unity_WorldToLight[3] * u_xlat0.wwww + u_xlat1;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToShadow[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToShadow[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD8 = hlslcc_mtx4x4unity_WorldToShadow[3] * u_xlat0.wwww + u_xlat1;
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 _LightShadowData;
uniform 	vec4 _ShadowMapTexture_TexelSize;
uniform 	vec4 _LightColor0;
uniform 	vec4 _Diffuse_ST;
uniform 	float _Alpha_cutoff;
UNITY_LOCATION(0) uniform  sampler2D _Diffuse;
UNITY_LOCATION(1) uniform  sampler2D _LightTexture0;
UNITY_LOCATION(2) uniform  sampler2D _LightTextureB0;
UNITY_LOCATION(3) uniform  sampler2D _ShadowMapTexture;
UNITY_LOCATION(4) uniform  sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
in  vec2 vs_TEXCOORD0;
in  vec4 vs_TEXCOORD3;
in  vec3 vs_TEXCOORD4;
in  vec4 vs_TEXCOORD7;
in  vec4 vs_TEXCOORD8;
layout(location = 0) out vec4 SV_Target0;
vec4 u_xlat0;
vec4 u_xlat1;
bool u_xlatb1;
vec4 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
float u_xlat8;
vec2 u_xlat16;
float u_xlat21;
bool u_xlatb21;
void main()
{
    u_xlat0.xy = vs_TEXCOORD0.xy * _Diffuse_ST.xy + _Diffuse_ST.zw;
    u_xlat0 = texture(_Diffuse, u_xlat0.xy);
    u_xlat21 = u_xlat0.w * _Alpha_cutoff + -0.5;
    u_xlatb21 = u_xlat21<0.0;
    if(((int(u_xlatb21) * int(0xffffffffu)))!=0){discard;}
    u_xlat1.xyz = vs_TEXCOORD8.xyz / vs_TEXCOORD8.www;
    u_xlat2.xy = u_xlat1.xy * _ShadowMapTexture_TexelSize.zw + vec2(0.5, 0.5);
    u_xlat2.xy = floor(u_xlat2.xy);
    u_xlat1.xy = u_xlat1.xy * _ShadowMapTexture_TexelSize.zw + (-u_xlat2.xy);
    u_xlat16.xy = (-u_xlat1.xy) + vec2(1.0, 1.0);
    u_xlat3.xy = min(u_xlat1.xy, vec2(0.0, 0.0));
    u_xlat3.xy = (-u_xlat3.xy) * u_xlat3.xy + u_xlat16.xy;
    u_xlat4.y = u_xlat3.x;
    u_xlat16.xy = max(u_xlat1.xy, vec2(0.0, 0.0));
    u_xlat5 = u_xlat1.xxyy + vec4(0.5, 1.0, 0.5, 1.0);
    u_xlat3.xz = (-u_xlat16.xy) * u_xlat16.xy + u_xlat5.yw;
    u_xlat16.xy = u_xlat5.xz * u_xlat5.xz;
    u_xlat4.z = u_xlat3.x;
    u_xlat1.xy = u_xlat16.xy * vec2(0.5, 0.5) + (-u_xlat1.xy);
    u_xlat4.x = u_xlat1.x;
    u_xlat3.x = u_xlat1.y;
    u_xlat4.w = u_xlat16.x;
    u_xlat3.w = u_xlat16.y;
    u_xlat3 = u_xlat3 * vec4(0.444440007, 0.444440007, 0.444440007, 0.222220004);
    u_xlat4 = u_xlat4 * vec4(0.444440007, 0.444440007, 0.444440007, 0.222220004);
    u_xlat5 = u_xlat4.ywyw + u_xlat4.xzxz;
    u_xlat1.xy = u_xlat4.yw / u_xlat5.zw;
    u_xlat1.xy = u_xlat1.xy + vec2(-1.5, 0.5);
    u_xlat4.xy = u_xlat1.xy * _ShadowMapTexture_TexelSize.xx;
    u_xlat6 = u_xlat3.yyww + u_xlat3.xxzz;
    u_xlat1.xy = u_xlat3.yw / u_xlat6.yw;
    u_xlat3 = u_xlat5 * u_xlat6;
    u_xlat1.xy = u_xlat1.xy + vec2(-1.5, 0.5);
    u_xlat4.zw = u_xlat1.xy * _ShadowMapTexture_TexelSize.yy;
    u_xlat5 = u_xlat2.xyxy * _ShadowMapTexture_TexelSize.xyxy + u_xlat4.xzyz;
    u_xlat2 = u_xlat2.xyxy * _ShadowMapTexture_TexelSize.xyxy + u_xlat4.xwyw;
    vec3 txVec0 = vec3(u_xlat5.xy,u_xlat1.z);
    u_xlat21 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    vec3 txVec1 = vec3(u_xlat5.zw,u_xlat1.z);
    u_xlat1.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec1, 0.0);
    u_xlat1.x = u_xlat1.x * u_xlat3.y;
    u_xlat21 = u_xlat3.x * u_xlat21 + u_xlat1.x;
    vec3 txVec2 = vec3(u_xlat2.xy,u_xlat1.z);
    u_xlat1.x = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec2, 0.0);
    vec3 txVec3 = vec3(u_xlat2.zw,u_xlat1.z);
    u_xlat8 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec3, 0.0);
    u_xlat21 = u_xlat3.z * u_xlat1.x + u_xlat21;
    u_xlat21 = u_xlat3.w * u_xlat8 + u_xlat21;
    u_xlat1.x = (-_LightShadowData.x) + 1.0;
    u_xlat21 = u_xlat21 * u_xlat1.x + _LightShadowData.x;
    u_xlat1.xy = vs_TEXCOORD7.xy / vs_TEXCOORD7.ww;
    u_xlat1.xy = u_xlat1.xy + vec2(0.5, 0.5);
    u_xlat1 = texture(_LightTexture0, u_xlat1.xy);
    u_xlatb1 = 0.0<vs_TEXCOORD7.z;
    u_xlat1.x = u_xlatb1 ? 1.0 : float(0.0);
    u_xlat1.x = u_xlat1.w * u_xlat1.x;
    u_xlat8 = dot(vs_TEXCOORD7.xyz, vs_TEXCOORD7.xyz);
    u_xlat2 = texture(_LightTextureB0, vec2(u_xlat8));
    u_xlat1.x = u_xlat1.x * u_xlat2.x;
    u_xlat21 = u_xlat21 * u_xlat1.x;
    u_xlat1.xyz = vec3(u_xlat21) * _LightColor0.xyz;
    u_xlat2.xyz = _WorldSpaceLightPos0.www * (-vs_TEXCOORD3.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat21 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat2.xyz = vec3(u_xlat21) * u_xlat2.xyz;
    u_xlat21 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat3.xyz = vec3(u_xlat21) * vs_TEXCOORD4.xyz;
    u_xlat21 = dot(u_xlat3.xyz, u_xlat2.xyz);
    u_xlat21 = max(u_xlat21, 0.0);
    u_xlat1.xyz = u_xlat1.xyz * vec3(u_xlat21);
    SV_Target0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    SV_Target0.w = 0.0;
    return;
}

#endif
                             $GlobalsT         _WorldSpaceLightPos0                         _LightShadowData                        _ShadowMapTexture_TexelSize                          _LightColor0                  0      _Diffuse_ST                   @      _Alpha_cutoff                     P          $Globals@  
      _Time                            _TimeEditor                     
   _Amplitude                       
   _Windspeed                    $     __ST                  0     unity_WorldToShadow                       unity_ObjectToWorld                       unity_WorldToObject                  P     unity_MatrixVP                   �     unity_WorldToLight                   �            _Diffuse                  _LightTexture0                  _LightTextureB0                 _ShadowMapTexture                   _                