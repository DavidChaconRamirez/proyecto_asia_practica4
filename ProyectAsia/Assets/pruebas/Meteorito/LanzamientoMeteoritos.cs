﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanzamientoMeteoritos : MonoBehaviour
{
    public Transform prefab;
    public GameObject list;
    private bool verdad = true;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (verdad == true)
        {
            StartCoroutine(WaitAndPrint());
            verdad = false;
        }
        
    }

    private IEnumerator WaitAndPrint()
    {
        Instantiate(prefab, list.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(6f);
        verdad = true;
        

    }
}
