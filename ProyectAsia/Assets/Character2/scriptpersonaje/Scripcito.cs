﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Scripcito : MonoBehaviour
{

    public GameObject Superior;
    public GameObject Inferior;
    public GameObject Transicion;
    public GameObject Player;

    void Update()
    {
        if (GetComponent<PlayableDirector>().state != PlayState.Playing)
        {
            Superior.SetActive(false);
            Inferior.SetActive(false);
            Transicion.SetActive(false);
            Player.GetComponent<CharacterController>().enabled = true;
            Player.GetComponent<PlayerTPSController>().enabled = true;
            Player.GetComponent<CharacterAnimBasedMovement>().enabled = true;
        }
        else {
            Superior.SetActive(true);
            Inferior.SetActive(true);
            Transicion.SetActive(true);
            Player.GetComponent<CharacterController>().enabled = false;
            Player.GetComponent<PlayerTPSController>().enabled = false;
            Player.GetComponent<CharacterAnimBasedMovement>().enabled = false;
        }
    }
}
